﻿using Domain;
using MediatR;
using Persistence;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Provinces
{
    public class Details
    {
        public class Query: IRequest<Province>
        {
            public Guid Id { get; set; }
        }

        public class Handler : IRequestHandler<Query, Province>
        {
            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }

            public async Task<Province> Handle(Query request, CancellationToken cancellationToken)
            {
                var province = await _context.Provinces.FindAsync(request.Id);

                return province;
            }
        }
    }
}
