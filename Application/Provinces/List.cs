﻿using MediatR;
using Domain;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using Persistence;
using Microsoft.EntityFrameworkCore;

namespace Application.Provinces
{
    public class List
    {
        public class Query : IRequest<List<Province>> { }

        public class Handler : IRequestHandler<Query, List<Province>>
        {
            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }
            public async Task<List<Province>> Handle(Query request, 
                                              CancellationToken cancellationToken)
            {
                var provinces = await _context.Provinces.ToListAsync();

                return provinces;
            }
        }
    }
}
