﻿using Domain;
using MediatR;
using Persistence;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Provinces
{
    public class Create
    {
        public class Command: IRequest
        {
            public Guid Id { get; set; }            
            public string Nom { get; set; }          
            public string Description { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var province = new Province
                {
                  Id = request.Id,
                  Nom = request.Nom,
                  Description = request.Description
                };

                _context.Provinces.Add(province);
                var success = await _context.SaveChangesAsync() > 0;

                if (success) return Unit.Value;

                throw new Exception("Problème d'enregistrement des modifications");
            }
        }
    }
}
