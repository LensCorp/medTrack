﻿using MediatR;
using Persistence;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Provinces
{
    public class Delete
    {

        public class Command : IRequest
        {
            public Guid Id { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            public Handler(DataContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var province = await _context.Provinces.FindAsync(request.Id);

                if (province == null)
                    throw new Exception("Impossible de trouver la province");

                _context.Remove(province);

                var success = await _context.SaveChangesAsync() > 0;                

                if (success) return Unit.Value;

                throw new Exception("Problème d'enregistrement des modifications!");
            }
        }
    }
}
