﻿using System;
using MediatR;
using Persistence;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Provinces
{
    public class Edit
    {

        public class Command : IRequest
        {
            public Guid Id { get; set; }
            public string Nom { get; set; }
            public string Description { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            public Handler(DataContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var province = await _context.Provinces.FindAsync(request.Id);

                if (province == null)
                    throw new Exception("Pouvex pas trouver la province!");

                //Update the province

                province.Nom = request.Nom ?? province.Nom;
                province.Description = request.Description ?? province.Description;

                var success = await _context.SaveChangesAsync() > 0;

                if (success) return Unit.Value;  // return an empty object

                throw new Exception("Problème d'enregistrement des modifications!");
            }
        }
    }
}
