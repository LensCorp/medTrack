﻿using Domain;
using System.Collections.Generic;
using System.Linq;

namespace Persistence
{
    public class Seed
    {
        public static void SeedData(DataContext context)
        {
            if (!context.Provinces.Any())
            {
                var province = new List<Province>
                {
                   new Province
                   {
                       Description = " Le Nord-Kivu est une province bordant le lac Kivu dans l'est de la République démocratique du Congo. Sa capitale est Goma. ",
                       Nom = "Nord-Kivu",
                   },
                   new Province
                   {
                       Description = " Le Sud-Kivu est l'une des 26 provinces de la République démocratique du Congo. Sa capitale est Bukavu. ",
                       Nom = "Sud-Kivu",
                   },
                   new Province
                   {
                       Description = " La nouvelle province a finalement été créée en 2015 à partir du district de Lulua et de la ville de Kananga administrée de façon indépendante, tous deux faisant auparavant partie de la province du Kasaï-Occidental antérieure à 2015. ",
                       Nom = "Kasaï Province",

                   },
                   new Province
                   {  
                       Description = "Activity 8 months in future",
                       Nom = "Kasaï-Central", 
                   },
                   new Province
                   {
                       Description = "Le Maniema est l'une des 26 provinces de la République démocratique du Congo. Sa capitale est Kindu. ",
                       Nom = "Maniema",

                   },
                   new Province
                   {
                       Description = "La province de l'Équateur est l'une des vingt-cinq provinces de la République démocratique du Congo spécifiées par l'article 2 de la Constitution du pays de 2006. ",
                       Nom = "Équateur",

                   },
                   new Province
                   {
                       Description = "Le Bas-Uele est l'une des 26 provinces de la République démocratique du Congo. Sa capitale est la ville de Buta.",
                       Nom = "Bas-Uele",

                   },
                   new Province
                   {
                      Description = "Tshopo est l'une des 26 provinces de la République démocratique du Congo.Il est situé dans le nord-est du pays sur la rivière Tshopo, pour laquelle il est nommé.Sa capitale est Kisangani.",
                       Nom = "Tshopo",

                   },
                   new Province
                   {
                       Description = "L'Ituri est l'une des 26 provinces de la République démocratique du Congo. Sa capitale est la ville de Bunia.",
                       Nom = "Ituri Province",

                   },
                   new Province
                   {
                       Description = "Kongo Central, anciennement Bas-Congo, est l'une des 26 provinces de la République démocratique du Congo. Sa capitale est Matadi.",
                       Nom = "Kongo Central",
                       
                   }
                };

                context.Provinces.AddRange(province);
                context.SaveChanges();
            }
        }
    }
}
