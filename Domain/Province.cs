﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Province
    {
        public Guid Id { get; set; }
        [MaxLength(50)]
        public string Nom { get; set; }
        [MaxLength(1000)]
        public string Description { get; set; }
    }
}
